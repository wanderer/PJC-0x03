#include "bubblesort.h"

void bubblesort(int a[], int elems){
    int a_length = sizeof(a)/sizeof(char);
    int no_more_cycles = 0;

    printf("[i] a_length = %d\n", a_length);

    printf("[*] array [%d]:\n", a_length);
    for (int j = 0; j < a_length; ++j) {
        printf("%d ", a[j]);
    }
    printf("\n");

    while (no_more_cycles != a_length) {
        no_more_cycles = 0;
        for (int i = 0; i < a_length; ++i) {
            if (a[i] > a[i+1]){
                int tmp = a[i];
                a[i] = a[i+1];
                a[i+1] = tmp;
            }
            else {
                no_more_cycles++;
            }
        }
    }

    printf("[*] bubblesorted array [%d]:\n", a_length);
    for (int i = 0; i < a_length; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int return_bubblesorted(int a[]){
	return 0;
}
