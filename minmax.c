#include <stdio.h>
#include "minmax.h"
#include "main.h"

int maximum()
{
    int x, y, z;
    printf("** printmax **\n");
    printf("Zadejte 3 cisla oddelena mezerou: ");

    scanf("%d%d%d", &x, &y, &z);
    int max = x;
    if ( y > max ) {
        max = y;
    }
    if ( z > max ) {
        max = z;
    }
    return max;
}

int minimum()
{
    int selection, max, min = 0;


    int x, y, z;
    printf("** printmin **\n");
    printf("Zadejte 3 cisla oddelena mezerou: ");

    scanf("%d%d%d", &x, &y, &z);
    min = x;
    if ( y < min ) {
        min = y;
    }
    if ( z < min ) {
        min = z;
    }
    return min;
}