#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "minmax.h"
#include "bubblesort.h"

int get_userguess(){
    int x = -1;
    int ok = -1;
    while( ok != 1){
        ok = scanf("%d", &x);
        if(ok == EOF){
            printf("\ncaught Ctrl+D...\n");
            ok = 1;
            x = 2;
            break;
        }
        if(ok == 0){
            while (fgetc(stdin) != '\n'){
            }
            printf("[*] give me a correct value\n");
            break;
//            printf("[*] stop gibbering around\n    my guess >>> ");
        }
    }
    return x;
}

int lemmego(){
    printf("[*] you wished to go, quitting...\n");
    return EXIT_SUCCESS;
}

int main()
{
    int selection, max, min;
    int a[] = {1,2,3,5,4,0};

    while ( selection != 8 ) {
        printf("\npick a choice\n");
        printf("1 - max\n"
               "2 - min\n"
               "3 - bubblesort\n"
               "4 - nothing here yet\n"
               "8 - quit\n"
               ">> ");

        selection = get_userguess();

        switch ( selection ) {
            case 1:
                max = maximum();
                printf("maximum je %d\n", max);
                break;
            case 2:
                min = minimum();
                printf("minumum je %d\n", min);
                break;
            case 3:
		bubblesort(a,6);
                break;
            case 4:
                break;
            case 8:
                lemmego();
                return 9000;
            default:
                printf("[*] invalid option\n");
                selection = 0;
                break;
        }
    }
    return 0;
}
