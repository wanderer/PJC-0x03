#ifndef PJC_0X03_BUBBLESORT_H
#define PJC_0X03_BUBBLESORT_H

#include <stdio.h>

void bubblesort(int a[], int elems);

int return_bubblesorted(int a[]);

#endif //PJC_0X03_BUBBLESORT_H
